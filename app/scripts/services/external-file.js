
/*
 * Ex File :D
 * service for extracting external file text
 */
app.service('ExFile', function($http, $q) {
    var _this = this;

    this.promiseToHaveData = function(file, successFunc) {
        var defer = $q.defer();

        $http.get(file)
            .success(function(data) {
                angular.extend(_this, data);
                defer.resolve();
                successFunc(data);
            })
            .error(function() {
                defer.reject('could not find '+file);
            });

        return defer.promise;
    };
    
});
