'use strict';

/**
 * @ngdoc function
 * @name netcraftTaskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the netcraftTaskApp
 */
angular.module('netcraftTaskApp')
  .controller('MainCtrl', function ($scope, $window, $sce, $timeout, ExFile) {
  	
  	
  	/*
  	 * ---------------
  	 * begin private functions
  	 * ---------------
  	 */
  	
  	/*
  	 * init - called once when page is loaded
  	 */
	var _init = function(){
		
		// sets tabframes collection for the tabs' contents
		$scope.showIframe = true;
		$scope.tabFrames = {
			reports:{
				item: 'Report',
				open:false,
				custom: true,
				urls: angular.fromJson(localStorage.reports)
			},
			folders:{
				open:false,
				custom: false,
				urls: [{url:'http://www.netcraft.co.il/'}]
			},
			teamFolders:{
				item: 'Folder',
				open:false,
				custom: true,
				urls: angular.fromJson(localStorage.teamFolders)
			},
			publickReport:{
				open:false,
				custom: false,
				urls: [{url:'http://www.netcraft.co.il/'}]
			}
		};
		
		// call opentab to set the first tab opened
		$scope.openTab('reports');
		
		// alert text from an external file
		ExFile.promiseToHaveData('someFile.json', function(data){
			alert(data.text);
		});
	};
	
	/*
	 * sets the forlderList obj
	 * used the show the urls in the settings form
	 * gets values from the urlList obj if exists
	 */
	var _setFolders = function(){
		var urls = $scope.urlList ? $scope.urlList : [];
		$scope.FolderList = [];
		for (var i=0; i < 3; i++){
			$scope.FolderList[i] = {
				name: urls[i] ? urls[i].name : '',
				url: urls[i] ? urls[i].url : ''
			};
		}
	};
	
	/*
	 * goes through the folderlist
	 * and returns a list of only the filled in values
	 */
	var _getFolders = function(){
		var urls = [];
		for (var i=0; i < $scope.FolderList.length; i++){
			if ($scope.FolderList[i].url && $scope.FolderList[i].name){
				urls.push($scope.FolderList[i]);
			}
		}
		return urls;
	};
	
	/*
	 * sets the required validation in the settings form dynamically
	 * runs when the user changes a text input
	 * (so the input in the same row will be required or not)
	 */
	$scope.setValidations = function(){
		var minField = false;
		for (var i=0; i < $scope.FolderList.length; i++){
			var name = $("#name-"+i)[0];
			var url = $("#url-"+i)[0];
			if (url.value !=''){
				name.required = true;
				minField = true;
			}
			else{
				name.required = false;
			}
			if (name.value !=''){
				url.required = true;
				minField = true;
			}
			else{
				url.required = false;
			}
		}
		if (!minField){
			$("#name-0")[0].required = true;
			$("#url-0")[0].required = true;
		}
	};
	
	var _getSelected = function(){
		var index = 0;
		for (var i=0; i < $scope.urlList.length; i++){
			if ($scope.urlList[i].selected){
				index = i;
				break;
			}
		}
		return index;
	};
	
	/*
	 * runs when the user opens a new tab or changes the settings
	 * sets the content according to the tab's settings
	 */
	var _setFrame = function(){
		$scope.urlList = ($scope.tabFrames[$scope.currentTab].urls) ? ($scope.tabFrames[$scope.currentTab].urls) : [] ;
		var hasUrls = ($scope.urlList[0]!=undefined&&$scope.urlList[0].url) ? true : false;
		var isCustom = $scope.tabFrames[$scope.currentTab].custom;
		
		if (hasUrls) {
			var selectedIndex = _getSelected();
			$scope.selectedUrl = $scope.urlList[selectedIndex];
		}
		else if (isCustom) {
			$scope.selectedUrl = null;
			$scope.openSettings();
		}
		
		$scope.showSettingsButton = isCustom;
		$scope.showSelect = (hasUrls&&isCustom);
		$scope.showExpand = hasUrls;
		$scope.showIframe = hasUrls;
	};
	
  	/*
  	 * ---------------
  	 * end private functions
  	 * ---------------
  	 */
  	
  	
  	/*
  	 * ---------------
  	 * begin scope functions
  	 * ---------------
  	 */
  	
	/*
	 * runs when the user changes the select box option
	 * saves the selected propety in the urlList and local storage
	 */
	$scope.changeSelect = function(){
		// unselect all
		for (var key in $scope.urlList) {
		   $scope.urlList[key].selected = false;
		}
		var newIndex = $scope.urlList.indexOf($scope.selectedUrl);
		$scope.urlList[newIndex].selected = true;
		// save changes
		$scope.tabFrames[$scope.currentTab].urls = $scope.urlList;
		localStorage[$scope.currentTab] = angular.toJson($scope.urlList);
	};
	
	/*
	 * runs when the selected url changes
	 * on change tabs/ select box choice
	 * if the it is a customized tab save the choice index to open next time
	 */
	$scope.$watchCollection('selectedUrl', function(newVal, oldVal){
		if (newVal){
			$scope.currentUrl = $sce.trustAsResourceUrl(newVal.url);
		}
	});
	
  	/*
  	 * runs when clicking a tab button
  	 * close all tabs
  	 * open clicked tab and save selection
  	 * call setframe to set the content view
  	 */
	$scope.openTab = function(tab){
		// close all tabs
		for (var key in $scope.tabFrames) {
		   $scope.tabFrames[key].open = false;
		}
		// set chosen tab
		$scope.tabFrames[tab].open = true;
		$scope.currentTab = tab;
		_setFrame();
	};
	
	/*
	 * displays the settings form
	 * calls setfolders to sets the form values
	 * calls set validation
	 */
	$scope.openSettings = function(){
		$scope.showSettings = true;
		_setFolders();
		$timeout(function(){
			$scope.setValidations();
		});
    };
    
    /*
     * submits the settings form
     * checks form validity
     * if form is valid set the new url list in the current tab's property
     *  save the list in local storage
     * call setframe to reset the view
     */
	$scope.saveSettings = function(){
		var f = $('form')[0];
		 if(f.checkValidity()) {
			var tempUrls = _getFolders();
			if (tempUrls.length > 0){
				$scope.showSettings = false;
				tempUrls[0].selected = true;
				$scope.tabFrames[$scope.currentTab].urls = tempUrls;
				localStorage[$scope.currentTab] = angular.toJson(tempUrls);
				_setFrame();
			}
		 }
    };
    
    // open url in new tab
	$scope.redirect = function(url){
		$window.open(url);
    };
  	/*
  	 * ---------------
  	 * end scope functions
  	 * ---------------
  	 */
    
	 /*
	  * click anywhere event
	  * close settings
	  */
    $('html').click(function() {
    	if ($scope.selectedUrl){
	        $scope.showSettings = false;
	        $scope.$apply();
       }
    });
    // stop the event when click on settings
    $('.settings').click(function(event){
        event.stopPropagation();
    });  
    $('#settings-button').click(function(event){
        event.stopPropagation();
    });  
    
    
    _init();
    
});
