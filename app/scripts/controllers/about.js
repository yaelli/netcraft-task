'use strict';

/**
 * @ngdoc function
 * @name netcraftTaskApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the netcraftTaskApp
 */
angular.module('netcraftTaskApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
